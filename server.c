/*
 * tcpserver.c - A simple TCP echo server
 * usage: tcpserver <port>
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <openssl/md5.h>

#define BUFSIZE 1024

/*
 * error - wrapper for perror
 */
void error(char *msg) {
    perror(msg);
    exit(1);
}

int main(int argc, char **argv) {
    int parentfd; /* parent socket */
    int childfd; /* child socket */
    int portno; /* port to listen on */
    int clientlen; /* byte size of client's address */
    struct sockaddr_in serveraddr; /* server's addr */
    struct sockaddr_in clientaddr; /* client addr */
    struct hostent *hostp; /* client host info */
    char buf[BUFSIZE]; /* message buffer */
    char *hostaddrp; /* dotted decimal host addr string */
    int optval; /* flag value for setsockopt */
    int n; /* message byte size */
    int fSize, read_return, filefd;
    char fName[100];
    MD5_CTX mdContext;
    unsigned char data_[102];
    int bytes, i;
    unsigned char c[MD5_DIGEST_LENGTH];
    /*
     * check command line arguments
     */
    if (argc != 2) {
        fprintf(stderr, "usage: %s <port>\n", argv[0]);
        exit(1);
    }
    portno = atoi(argv[1]);

    /*
     * socket: create the parent socket
     */
    parentfd = socket(AF_INET, SOCK_STREAM, 0);
    if (parentfd < 0)
        error("ERROR opening socket");

    /* setsockopt: Handy debugging trick that lets
     * us rerun the server immediately after we kill it;
     * otherwise we have to wait about 20 secs.
     * Eliminates "ERROR on binding: Address already in use" error.
     */
    optval = 1;
    setsockopt(parentfd, SOL_SOCKET, SO_REUSEADDR,
               (const void *)&optval , sizeof(int));

    /*
     * build the server's Internet address
     */
    bzero((char *) &serveraddr, sizeof(serveraddr));

    /* this is an Internet address */
    serveraddr.sin_family = AF_INET;

    /* let the system figure out our IP address */
    serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);

    /* this is the port we will listen on */
    serveraddr.sin_port = htons((unsigned short)portno);

    /*
     * bind: associate the parent socket with a port
     */
    if (bind(parentfd, (struct sockaddr *) &serveraddr,
             sizeof(serveraddr)) < 0)
        error("ERROR on binding");

    /*
     * listen: make this socket ready to accept connection requests
     */
    if (listen(parentfd, 5) < 0) /* allow 5 requests to queue up */
        error("ERROR on listen");
    printf("Server Running ....\n");
    /*
     * main loop: wait for a connection request, echo input line,
     * then close connection.
     */
    clientlen = sizeof(clientaddr);
    while (1) {

        /*
         * accept: wait for a connection request
         */
        childfd = accept(parentfd, (struct sockaddr *) &clientaddr, &clientlen);
        if (childfd < 0)
            error("ERROR on accept");
        /*
         * read: read input string from the client
         */
        bzero(buf, BUFSIZE);
        n = read(childfd, buf, BUFSIZE);
        if (n < 0)
            error("ERROR reading from socket");
        sscanf(buf, "%d %s", &fSize, fName);
        bzero(buf, BUFSIZE);
        sprintf(buf, "server_%s", fName);
        sprintf(fName, "%s", buf);
        bzero(buf, BUFSIZE);
        sprintf(buf, "ACK");
        /*
         * write: echo the input string back to the client
         */
        n = write(childfd, buf, strlen(buf));
        if (n < 0)
            error("ERROR writing to socket");

        bzero(buf, BUFSIZE);
        filefd = open(fName,
                      O_WRONLY | O_CREAT | O_TRUNC,
                      S_IRUSR | S_IWUSR);
        if (filefd == -1) {
            ("Couldn't open file");
        }
        do {
            read_return = read(childfd, buf, BUFSIZE);
            if (read_return == -1) {
                error("Couldn't read from socket");
            }
            if (write(filefd, buf, read_return) == -1) {
                error("Couldn't write to file");
            }
            fSize -= read_return;
            if (fSize == 0) {
                break;
            }
        } while (read_return > 0);
        close(filefd);

        FILE *inFile = fopen (fName, "rb");
        MD5_Init (&mdContext);
        while ((bytes = fread (data_, 1, 102, inFile)) != 0)
            MD5_Update (&mdContext, data_, bytes);
        MD5_Final (c,&mdContext);
        fclose (inFile);
        bzero(buf, BUFSIZE);
        sprintf(buf, "%s", c);
        n = write(childfd, buf, strlen(buf));
        if (n < 0)
            error("ERROR writing to socket");
        close(childfd);
    }
}
