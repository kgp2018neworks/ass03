all: server client

server: server.c
	gcc server.c -o server -lcrypto

client: client.c
	gcc client.c -o client -lcrypto

clean:
	rm server client

runServer:
	./server 2048

runClient:
	./client 127.0.0.1 2048 mongoose.pdf

